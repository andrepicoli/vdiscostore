#VDisco API

**This API was create to show my experience about Java and Rest.**

Let's start?

First of all, you need to download this project in the link below

**git clone git@bitbucket.org:andrepicoli/vdiscostore.git**

After that, you need to install every maven dependencies. In your console, write the follow code.

**mvn clean install -DSkipTests=true**

**mvn eclipse\:eclipse**

Now we have all we need to run V DIsco API.

After Starting, let's create our database. Create on PostgreSQL a database with name: vdisco

Hibernate will create the tables automatically.

To fill cashback table, we have to options:

1 - Using cashback endpoint with POST method - http://localhost:8080/cashbacks
Json example:

{
	"genre":"ROCK",
	"percent": "40",
	"weekDay": "SUNDAY"
}

2 - Because I'm a good guy, I created the entire script to you! ;) ... It's in script.sql archive, in project root.

After creating our cashback table, we can search albums on Spotify API using url (as an example):

**http://localhost:8080/discos?genre=mpb**

And

Find a specific one using url:

**http://localhost:8080/discos/14xCfgcBE9uCrwwR36NoVB**

When you select what you want, you can go to buy albums

**http://localhost:8080/sales**

Using POST method, you will select your choices and make a purchase.

Example of POST Method

{
  "costumerName": "Test Name",
  "discos": [
  	{
        "spotify_id": "44Ig8dzqOkvkGDzaUof9lK",
        "name": "Led Zeppelin IV (Deluxe Edition)",
        "genre": "classic"
    },
    {
        "spotify_id": "19lEZSnCCbVEkKchoPQWDZ",
        "name": "Aerosmith",
        "genre": "classic"
    },
    {
        "spotify_id": "5Dbax7G8SWrP9xyzkOvy2F",
        "name": "The Wall",
        "genre": "classic"
    },
    {
        "spotify_id": "5zj7hdfXIpN0OV2aFbk27J",
        "name": "Melim",
        "genre": "mpb"
    }
	]
}

API will calculate cashback based on weekday and genre of current day.

So, that's it!
 
 Thanks for the opportunity!