package com.vdiscostore.apispotify;

import java.io.IOException;
import java.time.DayOfWeek;
import java.util.Calendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.vdiscostore.domain.Disco;
import com.vdiscostore.domain.Token;

public class GetAlbumByIdSpotifyAPI {

	private static String MARKET = "market=BR";

	private static HttpClient client = HttpClients.createDefault();
	private static HttpGet httpget;
	private static String responseString;
	private static HttpResponse response;
	private static JsonParser parser = new JsonParser();
	private static JsonElement jsonobj;
	private static Disco disco;

	public static Disco getAlbumById(Token token, String id) throws ClientProtocolException, IOException {

		try {
		
			httpget = new HttpGet("https://api.spotify.com/v1/albums/"+id+"?"+MARKET);

			httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
			httpget.addHeader("Content-Type","application/json");

			try {
				response = client.execute(httpget);
				responseString = new BasicResponseHandler().handleResponse(response);
			} catch (Exception e) {
				e.printStackTrace();
			}

			parser = new JsonParser(); 
			jsonobj = (JsonObject) parser.parse(responseString);
		
		} catch (JsonSyntaxException e1) {
			
			//If token expired, get a new one
			GetTokenSpotifyAPI getToken = new GetTokenSpotifyAPI();
			token = getToken.token;
			
			httpget = new HttpGet("https://api.spotify.com/v1/albums/"+id+"?"+MARKET);

			httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
			httpget.addHeader("Content-Type","application/json");

			try {
				response = client.execute(httpget);
				responseString = new BasicResponseHandler().handleResponse(response);
			} catch (Exception e) {
				e.printStackTrace();
			}

			parser = new JsonParser(); 
			jsonobj = (JsonObject) parser.parse(responseString);

		}

		//Constructor to new Object
		disco = new Disco();
		disco.setSpotify_id(jsonobj.getAsJsonObject().get("id").getAsString());
		disco.setName(jsonobj.getAsJsonObject().get("name").getAsString());
		double price = (double) Math.round(Math.random()*10);
		disco.setPrice(price);

		JsonParser parser = new JsonParser();
		jsonobj = (JsonObject) parser.parse(responseString);
		jsonobj = ((JsonObject) jsonobj).get("artists");
		
		JsonArray jsonArray = jsonobj.getAsJsonArray();
		
		String url = jsonArray.get(0).getAsJsonObject().get("href").getAsString();

		httpget = new HttpGet(url+"?"+MARKET);

		httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
		httpget.addHeader("Content-Type","application/json");

		try {
			response = client.execute(httpget);
			responseString = new BasicResponseHandler().handleResponse(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jsonobj = (JsonObject) parser.parse(responseString);
		jsonobj = ((JsonObject) jsonobj).get("genres");
		
		JsonArray jRay = new JsonArray();
		
		jRay = jsonobj.getAsJsonArray();
		
		String genre = "";
		
		for (JsonElement jsonElement : jRay) {
			if(jsonElement.getAsString().contains("pop")) {
				genre = "pop";
				break;
			}else if(jsonElement.getAsString().contains("rock")) {
				genre = "rock";
				break;
			}else if(jsonElement.getAsString().contains("mpb")) {
				genre = "mpb";
				break;
			}else if(jsonElement.getAsString().contains("classic")) {
				genre = "classic";
				break;
			}
		}
		
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK); 

		String toDay = DayOfWeek.of(day-1).toString();
		
		/**
		 * It was necessary cause sometimes genre comes as 'pop rock'...
		 * and I need to get the correct in cashback table the right value
		 */

		
		disco.setGenre(genre);

		url = "http://localhost:8080/cashbacks?weekday="+toDay.toUpperCase()+"&genre="+genre.toUpperCase();

		httpget = new HttpGet(url);
		response = client.execute(httpget);
		responseString = new BasicResponseHandler().handleResponse(response);

		parser = new JsonParser(); 
		jsonobj = (JsonObject) parser.parse(responseString);

		Double percent = Double.parseDouble(jsonobj.getAsJsonObject().get("percent").getAsString());

		disco.setCashback((disco.getPrice() * percent)/100);

		return disco;

	}

}
