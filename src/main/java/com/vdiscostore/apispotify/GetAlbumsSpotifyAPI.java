package com.vdiscostore.apispotify;

import java.io.IOException;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vdiscostore.domain.Disco;
import com.vdiscostore.domain.Token;

public class GetAlbumsSpotifyAPI {

	private static JsonArray jsonArray;
	private static String MARKET = "market=BR";
	private static List<Disco> discos = new ArrayList<Disco>();
	private static Disco disco;
	private static HttpClient client = HttpClients.createDefault();

	public static List<Disco> getAlbumSpotifyAPI(String genre, Token token) throws ClientProtocolException, IOException {

		HttpGet httpget;
		String responseString;
		JsonElement jsonobj;
		HttpResponse response;
		List<String> albums = new ArrayList<String>();

		try {
			//Creating and calling Spotify API filter tracks by genre
			
			httpget = new HttpGet("https://api.spotify.com/v1/search?q=genre:"+genre+"&type=track&limit=50&"+MARKET);

			httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
			httpget.addHeader("Content-Type","application/json");

			responseString = "";

			jsonobj = null;

			response = client.execute(httpget);
			responseString = new BasicResponseHandler().handleResponse(response);

		} catch (Exception e) {
			
			//If token expired, get a new one
			GetTokenSpotifyAPI getToken = new GetTokenSpotifyAPI();
			token = getToken.token;

			httpget = new HttpGet("https://api.spotify.com/v1/search?q=genre:"+genre+"&type=track&limit=50&"+MARKET);

			httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
			httpget.addHeader("Content-Type","application/json");

			responseString = "";

			jsonobj = null;

			try {
				response = client.execute(httpget);
				responseString = new BasicResponseHandler().handleResponse(response);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		JsonParser parser = new JsonParser(); 
		jsonobj = (JsonObject) parser.parse(responseString);
		jsonobj = ((JsonObject) jsonobj).get("tracks");
		jsonobj = ((JsonObject) jsonobj).get("items");

		//Creating jsonArray to loop and get tracks' albums
		jsonArray = jsonobj.getAsJsonArray();

		for (JsonElement jsonElement : jsonArray) {
			if(!jsonElement.isJsonNull()) {
				JsonElement jElement = null;
				jElement = jsonElement.getAsJsonObject().get("album");
				String album = jElement.getAsJsonObject().get("href").toString();
				album = album.replace("\"","");
				albums.add(album);
			}
		}

		parser = null;
		responseString = null;

		discos = new ArrayList<Disco>();
		
		for (String url : albums) {

			url = url+"?"+MARKET;
			httpget = new HttpGet(url);

			httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
			httpget.addHeader("Content-Type","application/json");

			response = client.execute(httpget);
			responseString = new BasicResponseHandler().handleResponse(response);

			parser = new JsonParser(); 
			jsonobj = (JsonObject) parser.parse(responseString);

			//Constructor to new Object
			disco = new Disco();
			disco.setSpotify_id(jsonobj.getAsJsonObject().get("id").getAsString());
			disco.setName(jsonobj.getAsJsonObject().get("name").getAsString());
			disco.setGenre(genre);

			//Setting price
			double price = (double) Math.round(Math.random()*10);
			disco.setPrice(price);
			
			//Finding percentual of genre and current day
			Calendar calendar = Calendar.getInstance();
			int day = calendar.get(Calendar.DAY_OF_WEEK); 

			String toDay = DayOfWeek.of(day-1).toString();

			url = "http://localhost:8080/cashbacks?weekday="+toDay.toUpperCase()+"&genre="+genre.toUpperCase();

			httpget = new HttpGet(url);
			response = client.execute(httpget);
			responseString = new BasicResponseHandler().handleResponse(response);

			parser = new JsonParser(); 
			jsonobj = (JsonObject) parser.parse(responseString);

			Double percent = Double.parseDouble(jsonobj.getAsJsonObject().get("percent").getAsString());

			disco.setCashback((disco.getPrice() * percent)/100);
			
			//Adding Discos inside of an Array
			discos.add(disco);

			disco = null;

		}	

		return discos;

	}
}
