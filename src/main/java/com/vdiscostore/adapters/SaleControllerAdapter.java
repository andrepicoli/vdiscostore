package com.vdiscostore.adapters;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vdiscostore.domain.Disco;
import com.vdiscostore.domain.Sale;
import com.vdiscostore.ports.SaleRepositoryPort;
import com.vdiscostore.repository.SaleRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
public class SaleControllerAdapter implements SaleRepositoryPort<Sale>{

    @Autowired
    private SaleRepository saleRepository;
	
    private static HttpClient client = HttpClients.createDefault();
	private static HttpGet httpget;
	private static HttpResponse response;
	private static String responseString;
	private static JsonParser parser;
	private static JsonObject jsonobj;

	private Double totalCashBack;
    
    @GetMapping("/sales")
    public List<Sale> getAllSales(Pageable pageable, @RequestParam String initialDate, @RequestParam String finalDate) throws ParseException {
    	Date init = new SimpleDateFormat("yyyy-MM-dd").parse(initialDate);
    	Date end = new SimpleDateFormat("yyyy-MM-dd").parse(finalDate);
    	return saleRepository.findAllWithCreationDate(init, end);
    }
    
    @GetMapping("/sales/{id}")
    public Optional<Sale> getByIdSales(@Valid @PathVariable Long id) {
    	return saleRepository.findById(id);
    }
    
    @PostMapping("/sales")
    public Sale saleADisc(@Valid @RequestBody Sale sale, Pageable pageable) throws BadHttpRequest, ClientProtocolException, IOException {
        if(sale == null) {
        	throw new BadHttpRequest();
        }
        
        List<Disco> discos = sale.getDiscos();
        
        Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK); 
        
		String toDay = DayOfWeek.of(day-1).toString();

		String url = "";
		
		totalCashBack = 0.00;
		
        for (Disco disco : discos) {
			
			url = "http://localhost:8080/cashbacks?weekday="+toDay.toUpperCase()+"&genre="+disco.getGenre().toUpperCase();

			httpget = new HttpGet(url);
			response = client.execute(httpget);
			responseString = new BasicResponseHandler().handleResponse(response);

			parser = new JsonParser(); 
			jsonobj = (JsonObject) parser.parse(responseString);
			
			if(disco.getPrice() == null || disco.getPrice().equals("")) {
				double price = (double) Math.round(Math.random()*10);
				disco.setPrice(price);
			}

			Double percent = Double.parseDouble(jsonobj.getAsJsonObject().get("percent").getAsString());

			Double cashback = (disco.getPrice() * percent)/100;
			
			disco.setCashback(cashback);
			
			totalCashBack += cashback;

		}
        
        sale.setCashbackTotal(totalCashBack);
        
        Date date = new Date();
        sale.setCreationDate(date);
    	
    	return saleRepository.save(sale);
    }
	
}
