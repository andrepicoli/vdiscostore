package com.vdiscostore.adapters;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vdiscostore.domain.CashBack;
import com.vdiscostore.ports.CashBackRepositoryPort;
import com.vdiscostore.repository.CashBackRepository;

@RestController
public class CashBackControllerAdapter implements CashBackRepositoryPort<CashBack>{

    @Autowired
    private CashBackRepository cashBackRepository;

    @GetMapping("/cashbacks")
    public CashBack getSpecificCashBack(@RequestParam("weekday") String weekDay, @RequestParam("genre") String genre, Pageable pageable) {
        return cashBackRepository.findOneByWeekDayAndGenre(weekDay, genre);
    }
    
    @GetMapping("/cashbacks/{id}")
    public Optional<CashBack> getCashBack(@PathVariable("id") Long id) {
        return cashBackRepository.findById(id);
    }
    
    @Override
    @PostMapping("/cashbacks")
    public void save(@Valid @RequestBody CashBack cashback) {
        cashBackRepository.save(cashback);
    }

}