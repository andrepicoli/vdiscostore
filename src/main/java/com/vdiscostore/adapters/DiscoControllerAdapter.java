package com.vdiscostore.adapters;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vdiscostore.apispotify.GetAlbumByIdSpotifyAPI;
import com.vdiscostore.apispotify.GetAlbumsSpotifyAPI;
import com.vdiscostore.apispotify.GetTokenSpotifyAPI;
import com.vdiscostore.domain.Disco;
import com.vdiscostore.domain.Token;

import javassist.NotFoundException;
import javassist.tools.web.BadHttpRequest;

@RestController
public class DiscoControllerAdapter {

	@GetMapping("/discos")
	public List<Disco> getDisco(@Valid @RequestParam String genre, Pageable pageable) throws Exception{

		if(genre == null && genre.equals("")) {
			throw new BadHttpRequest();
		}

		Token token = GetTokenSpotifyAPI.token;

		if(token == null || token.equals("")) {
			GetTokenSpotifyAPI getToken = new GetTokenSpotifyAPI();
			token = getToken.token;
		}

		return GetAlbumsSpotifyAPI.getAlbumSpotifyAPI(genre, token);

	}

	@GetMapping("/discos/{id}")
	public Disco getDiscoById(@PathVariable String id) throws Exception{

		Token token = GetTokenSpotifyAPI.token;

		if(token == null || token.equals("")) {
			GetTokenSpotifyAPI getToken = new GetTokenSpotifyAPI();
			token = getToken.token;
		}

		Disco disco = new Disco();
		
		disco = GetAlbumByIdSpotifyAPI.getAlbumById(token, id);

		if(disco == null) {
			throw new NotFoundException("Did not find a disco with id:"+id);
		}

		return disco;

	}

}
