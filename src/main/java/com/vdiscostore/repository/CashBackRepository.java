package com.vdiscostore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vdiscostore.domain.CashBack;

@Repository
public interface CashBackRepository extends JpaRepository<CashBack, Long> {

	CashBack findOneByGenre(String genre);
	
	CashBack findOneByWeekDayAndGenre(String weekDay, String genre);
	
}
