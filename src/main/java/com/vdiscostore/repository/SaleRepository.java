package com.vdiscostore.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vdiscostore.domain.Sale;

@Repository
public interface SaleRepository extends JpaRepository<Sale, Long> {

	@Query("select s from Sale s where creation_date >= :initialDate and creation_date <= :finalDate order by creation_date")
	List<Sale> findAllWithCreationDate(@Param("initialDate") Date initialDate, @Param("finalDate") Date finalDate);
	
}
