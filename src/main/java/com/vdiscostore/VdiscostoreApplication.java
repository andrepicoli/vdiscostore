package com.vdiscostore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VdiscostoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(VdiscostoreApplication.class, args);
	}

}

