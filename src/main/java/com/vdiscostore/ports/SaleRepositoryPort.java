package com.vdiscostore.ports;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.apache.http.client.ClientProtocolException;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

import javassist.tools.web.BadHttpRequest;

public interface SaleRepositoryPort<Sale> {

	List<Sale> getAllSales(Pageable pageable, String initialDate, String finalDate) throws ParseException;
	
	Optional<Sale> getByIdSales(Long id);
	
	Sale saleADisc(Sale sale, Pageable pageable) throws BadHttpRequest, ClientProtocolException, IOException;
	
}
