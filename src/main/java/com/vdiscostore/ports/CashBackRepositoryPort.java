package com.vdiscostore.ports;

import java.util.Optional;

import org.springframework.data.domain.Pageable;

public interface CashBackRepositoryPort<CashBack> {

	void save(CashBack cashback);
	
	Optional<CashBack> getCashBack(Long id);
	
	CashBack getSpecificCashBack(String weekDay, String genre, Pageable pageable);
	
}
