package com.vdiscostore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.RestAssured;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class VdiscostoreApplicationTests {

	private String BASE_URI = "http://localhost:8080";

	@Test
	public void shouldInsertACashBack() throws FileNotFoundException, JSONException {

		FileInputStream json = new FileInputStream(new File("src/test/java/com/vdiscostore/mock/cashback/insertCashBackMockup.json"));

		RestAssured rest = new RestAssured();

		rest.baseURI=BASE_URI;

		rest.given()
		.contentType("application/json")
		.and()
		.body(json)
		.when()
		.post("/cashbacks")
		.then()
		.statusCode(200);

	}
	
	@Test
	public void shouldGetACashBackById() throws FileNotFoundException, JSONException {

		RestAssured rest = new RestAssured();

		rest.baseURI=BASE_URI;

		rest.given()
		.contentType("application/json")
		.formParam("weekday", "SUNDAY")
		.formParam("genre", "POP")
		.when()
		.get("/cashbacks")
		.then()
		.statusCode(200);

	}

}

